import bibtexparser
import sys

with open(sys.argv[1]) as bibtex_file:
    bib_database = bibtexparser.load(bibtex_file)

print(bib_database.entries)
