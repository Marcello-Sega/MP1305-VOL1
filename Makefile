all: VOL1
# Bourgoin.bbl Brandt.bbl  Ekiel-Jezewska.bbl Marconi.bbl Sofonea.bbl Honciuc.bbl Icardi.bbl Ravnik.bbl Callegari.bbl
VOL1: Bourgoin.bbl Brandt.bbl Ekiel-Jezewska.bbl Marconi.bbl Sofonea.bbl Honciuc.bbl Icardi.bbl Ravnik.bbl Callegari.bbl
	pdflatex VOL1 ; while grep 'Rerun to get ' VOL1.log ; do pdflatex VOL1 ; done ; grep -i warning VOL1.log *.blg

%.bbl: %.aux 
	bibtex $<

%.aux: %.tex %.bib VOL1.tex
	pdflatex  VOL1
	pdflatex  VOL1

clean:
	rm -f VOL1.aux  VOL1.log  VOL1.out  VOL1.pdf VOL1.thm  VOL1.toc  
	find . -name "*.aux" -exec rm -f {} \; 
	find . -name "*.bbl" -exec rm -f {} \; 
	find . -name "*.blg" -exec rm -f {} \; 
	find . -name "*.log" -exec rm -f {} \; 
	find . -name "*.toc" -exec rm -f {} \; 

simple: 
	pdflatex VOL1
